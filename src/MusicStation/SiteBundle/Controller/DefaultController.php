<?php

namespace MusicStation\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     *
     * cache expires in 10 minutes
     * @Cache(expires="+10 minutes", smaxage="600")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        // get events sorting by ascending starting date
        $events = $em->getRepository('MusicStationUserBundle:Event')->findBy(
            array(),
            array('startDate' => 'ASC')
        );

        return array(
            'events' => $events
        );
    }

    /**
     * @Route("/about", name="about")
     * @Template()
     *
     * cache expires in 1 week
     * @Cache(expires="+1 week", smaxage="604800")
     */
    public function aboutAction()
    {
        return array();
    }
}
