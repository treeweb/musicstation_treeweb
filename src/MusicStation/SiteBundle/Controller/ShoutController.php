<?php

namespace MusicStation\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Response;


class ShoutController extends Controller
{
    /**
     * Print the latest shouts
     *
     * @Template()
     */
    public function _aside_latestsAction($limit = 3)
    {
        $em = $this->getDoctrine()->getManager();

        //$entities = $em->getRepository('MusicStationUserBundle:Shout')->findAllWithLimit($limit);

        $q = $em->createQueryBuilder()
            ->select('e')
            ->from('MusicStationUserBundle:Shout', 'e')
            ->orderBy('e.created', 'DESC')
            ->setMaxResults($limit)

            ->getQuery()
            ->setFetchMode('MusicStation\UserBundle\Entity\Shout', 'artist', \Doctrine\ORM\Mapping\ClassMetadataInfo::FETCH_EAGER)
        ;

        // doctrine cache
        $q->setResultCacheDriver(new \Doctrine\Common\Cache\ApcCache());
        $q->useResultCache(true, 60, '_aside_latests_shouts');

        $entities = $q->getResult();

        $response = new Response($this->renderView('MusicStationSiteBundle:Shout:_aside_latests.html.twig', array(
            'entities' => $entities
        )));

        // cache expires in 30 seconds
        $response->setSharedMaxAge(30);

        return $response;
    }

    /**
     * @Route("/shout/{id}", name="shout_show")
     * @Template()
     *
     * cache expires in 10 minutes
     * @Cache(expires="+10 minutes", smaxage="600")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MusicStationUserBundle:Shout')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Shout entity.');
        }

        $shoutPrev = $em->getRepository('MusicStationUserBundle:Shout')->getPrev($entity);
        $shoutNext = $em->getRepository('MusicStationUserBundle:Shout')->getNext($entity);

        return array(
            'entity' => $entity,
            'shout_prev' => $shoutPrev,
            'shout_next' => $shoutNext
        );
    }
}