<?php
namespace MusicStation\UserBundle\EventListener;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Form\Factory\FormFactory;
use FOS\UserBundle\Doctrine\UserManager;
use MusicStation\UserBundle\Entity\Artist;

/**
 * Class RegistrationListener
 * @package MusicStation\UserBundle\EventListener
 *
 * Listener for FOSUserEvents
 */
class RegistrationListener
{
    protected $userManager;
    protected $formFactory;

    public function __construct(UserManager $userManager, FormFactory $formFactory)
    {
        $this->userManager = $userManager;
        $this->formFactory = $formFactory;
    }

    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        // create user registration form
        $form = $this->formFactory->createForm();
        $form->bind($event->getRequest());

        // created User object
        $user = $event->getUser();

        /**
         * add Artist entity
         */
        $artist = new Artist();
        $artist->setName($form->get('artist_name')->getData());
        $artist->setBio($form->get('artist_bio')->getData());
        $artist->setUser($user);

        // set artist
        $user->setArtist($artist);

        // update user
        $this->userManager->updateUser($user);
    }
}